#!/usr/bin/env bash
set -e

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
# shellcheck source=_inc.sh
. "${BASE_DIR}/_inc.sh"

run_harvest() {
	(
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${HARVESTER_PROJ}" 'harvest' "$@"
	) >> "${LOG_FILE}" 2>&1

	HARVEST_SUCCESS="$?"

	if [ "${HARVEST_SUCCESS}" != '0' ]; then
		echo "WARNING (cron job script $0): harvest failed with exit status ${HARVEST_SUCCESS}. Will proceed to run VLO import."
	fi
}

run_vlo_import() {
	(
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${HARVESTER_PROJ}" 'viewer-import' "$@" &
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${VLO_PROJ}" 'run-import' &
		wait
	) >> "${LOG_FILE}" 2>&1

	IMPORT_SUCCESS="$?"
	
	if [ "${IMPORT_SUCCESS}" != '0' ]; then
		echo "ERROR (cron job script $0): import failed with exit status ${IMPORT_SUCCESS}"
		tail -n 100 "${LOG_FILE}"
		exit 1
	fi
}

linkchecker_is_running() {
	if [ -d "${CURATION_DIR}" ]; then
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${CURATION_PROJ}" 'status-link-checking' \
			   | grep -q "'linkchecker_running':[ ]*true"
		return "$?"
	else
		log_info "Curation directory does not exist. Assuming that link checker is NOT running."
		return 1
	fi
}

pause_linkchecking() {
	(
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${CURATION_PROJ}" 'stop-link-checking'
	) >> "${LOG_FILE}" 2>&1
}

resume_linkchecking() {
	(
		cd "${DEPLOY_HOME}" \
			&& bash 'control.sh' "${CURATION_PROJ}" 'start-link-checking'
	) >> "${LOG_FILE}" 2>&1
}

main() {
	LINKCHECKER_RUNNING_ON_START='false'
	
	if linkchecker_is_running; then
		log_info "Link checker is running"
		LINKCHECKER_RUNNING_ON_START='true'
	fi
	
	if [ "${LINKCHECKER_RUNNING_ON_START}" = 'true' ]; then
		log_info "Pausing link checker"
		# pause link checking while harvest is running
		pause_linkchecking \
			&& (run_harvest "$@"; 
			log_info "Resuming link checker"
			resume_linkchecking)
	else
		log_info "Link checker NOT running, will not pause and resume"
		# simply run harvest
		run_harvest "$@"
	fi

	run_vlo_import "$@"
}

main "$@"
