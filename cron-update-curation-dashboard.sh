#!/usr/bin/env bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "${BASE_DIR}/_inc.sh"

main() {
	(
		echo "$(date) - Triggering update of curation dashboard"
		cd "${DEPLOY_HOME}" \
		&& bash 'control.sh' "${CURATION_PROJ}" 'start-update-dashboard' \
		&& echo "$(date) - Update of curation dashboard started"
	) >> "${LOG_FILE}" 2>&1
	
	RESULT="$?"
	
	if [ "${RESULT}" != '0' ]; then
		log_error "Script ${BASH_SOURCE[0]} triggered by cron job failed with exit status ${RESULT}"
		echo -e "------------------------\nLast log content:\n------------------------"
		tail -n 500 "${LOG_FILE}"
		exit 1
	fi

}

main "$@"
