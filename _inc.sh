#!/usr/bin/env bash
DATE="$(/bin/date +"%Y%m%d")"

if ! [ "${LOG_FILE}" ]; then
  LOG_FILE="/var/log/vlo/import_and_harvest_${DATE}.log"
fi

if ! touch "${LOG_FILE}"; then
  (>&2 echo "Error: cannot write to log file '${LOG_FILE}'. Terminating." )
  exit 1
fi

DEPLOY_HOME="${DEPLOY_HOME:-/home/deploy}"

if ! [ -d "${DEPLOY_HOME}" ]; then
	(>&2 echo "Error: cannot find deploy home directory '${DEPLOY_HOME}'." )
	exit 1
fi

HARVESTER_DIR="${HARVESTER_DIR:-${DEPLOY_HOME}/harvester}"
VLO_DIR="${VLO_DIR:-${DEPLOY_HOME}/vlo}"
CURATION_DIR="${CURATION_DIR:-${DEPLOY_HOME}/curation}"

VLO_PROJ="$(basename "${VLO_DIR}")"
HARVESTER_PROJ="$(basename "${HARVESTER_DIR}")"
CURATION_PROJ="$(basename "${CURATION_DIR}")"

log_info() {
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] [INFO]" "$*" >> "${LOG_FILE}"
}

log_error() {
	# send to stdout, as well as log file!
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] [ERROR]" "$*" >> "${LOG_FILE}"
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] [ERROR]" "$*"
}
