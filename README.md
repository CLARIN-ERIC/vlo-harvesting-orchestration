# vlo-harvesting-orchestration

Scripts for orchestration of metadata harvesting, import and related process.

## Deployment

### Deployment using the deploy script

1. Become the deploy user and change to its home directory
2. Run the deploy script: `bash ./deploy.sh --name vlo-harvesting-orchestration-assets --git vlo-harvesting-orchestration --tag ${VERSION_TAG}`
3. Verify the updated state of the directory that is referenced in the crontab entries for the `cron-harvest-import.sh` and/or `cron-update-link-status.sh` scripts -- e.g. `/home/deploy/vlo-harvesting-orchestration/`

### Manual deployment 

1. Check state and structure of /home/deploy/vlo-harvesting-orchestration/. This may be a symlink; this path is used in the crontab, verify.
1. Download and expand the tarball `https://gitlab.com/CLARIN-ERIC/vlo-harvesting-orchestration/-/archive/${VERSION_TAG}/vlo-harvesting-orchestration-${VERSION_TAG}.tar.gz`
1. Put in place following the structure of the old deployment and replace the symlink.
1. Double check the existence and permissions of the scripts that are called in the crontab

## Configuration

You may **OPTIONALLY** set one or more of the following environment variables to change
the behaviour of the scripts or make them work in an environment that differs from the
deployment environments:

- `LOG_FILE`: file to which the scripts send their regular output (default: `/var/log/vlo/import_and_harvest_${DATE}.log`)
- `DEPLOY_HOME`: assumed 'deploy home' directory (default: `/home/deploy`)
- `HARVESTER_DIR`: directory where the harvester is deployed (default: `${DEPLOY_HOME}/harvester`)
- `VLO_DIR`: directory where the VLO is deployed (default: `${DEPLOY_HOME}/vlo`)
- `CURATION_DIR`: directory where the curation module/link checker is deployed (default: `${DEPLOY_HOME}/curation`)

