#!/usr/bin/env bash
set -e

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "${BASE_DIR}/_inc.sh"

main() {
	(
		cd "${DEPLOY_HOME}" \
		&& bash 'control.sh' "${VLO_PROJ}" 'run-link-status-update'
	) >> "${LOG_FILE}" 2>&1
	
	RESULT="$?"

	if [ "${RESULT}" != '0' ]; then
		echo "cron job failed with exit status ${RESULT}"
		cat "${LOG_FILE}"
		exit 1
	fi

}

main "$@"
